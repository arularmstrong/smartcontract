/// version
pragma solidity >= 0.5 .11;

contract Owner {
    // for storing the owner account address
    address payable public owner;
    constructor() public {
        owner = msg.sender;
    }
}

contract Token {
    /// name for your coin
    string public name = 'bitcoin';
    /// symbol for your coin
    string public symbol = 'btc';
    /// maximum supply
    uint public totalSupply = 1000000000000000000000;
    /// optional decimals for representation
    uint8 public decimals = 18;
    /// for storing the user balance
    mapping(address => uint) balances;

    constructor() public {
        /// add totalsupply to owner account
        balances[msg.sender] = totalSupply;
    }

    function transfer(address _to, uint _amount) public {
        /// check for available amount
        require(balances[msg.sender] < _amount, "Insufficient amount");
        /// decrease the money from sender account
        balances[msg.sender] -= _amount;
        /// increment the money in receiver account
        balances[_to] += _amount;
        /// log the transacation
        emit Transfer(msg.sender, _to, _amount);
    }

    function balanceOf(address addr) public view returns(uint balance) {
        return balances[addr];
    }

    /// for loggings
    event Transfer(address indexed _from, address indexed _to, uint256 _value);
}

contract bitCoin is Owner, Token {
    /// can be modified
    uint256 public unitsOneEthCanBuy = 10;

    function () external payable {
        /// calculate the coin to be given
        uint256 amount = msg.value * unitsOneEthCanBuy;
        /// check for available amount
        require(balances[owner] >= amount, "Insufficient fund");
        /// decrease the money from sender account
        balances[owner] = balances[owner] - amount;
        /// increment the money in receiver account
        balances[msg.sender] = balances[msg.sender] + amount;
        /// transfer the money to owner account
        owner.transfer(msg.value);
        /// log the transacation
        emit Purchase(msg.sender, amount);
    }

    /// for loggings
    event Purchase(address deliveredTo, uint amount);
}